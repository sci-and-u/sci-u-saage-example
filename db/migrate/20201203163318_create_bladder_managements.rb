class CreateBladderManagements < ActiveRecord::Migration[6.0]
  def change
    create_table :bladder_managements do |t|
      t.integer :response
      t.boolean :indwelling_catheter
      t.boolean :condom_catheter
      t.boolean :suprapubic_catheter
      t.boolean :intermittent_catheterization_urethra
      t.boolean :intermittent_catheterization_stoma
      t.boolean :ileal_conduit
      t.boolean :other
      t.text :other_details
      t.text :details
      t.bigint :saage_form_id

      t.timestamps
    end
  end
end
