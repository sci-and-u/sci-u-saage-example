class CreateBasicInformations < ActiveRecord::Migration[6.0]
  def change
    create_table :basic_informations do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :bp
      t.string :height
      t.string :weight
      t.date :dob
      t.bigint :saage_form_id

      t.timestamps
    end
  end
end
