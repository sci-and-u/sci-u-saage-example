Rails.application.routes.draw do

  resources :saage_forms
  resources :basic_informations
  resources :bladder_managements
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get '/my_care_plan' => 'pages#my_care_plan'
  get '/how_to_use' => 'pages#how_to_use'
  get '/about' => 'pages#about'
  
  root 'pages#my_care_plan'
end
