class SaageFormsController < ApplicationController
  before_action :set_saage_form, only: [:update, :destroy]  
  # GET /saage_forms
  # GET /saage_forms.json

  # GET /saage_forms/new
  def new
    @saage_form = SaageForm.new
    @saage_form.build_basic_information
    @saage_form.build_bladder_management
  end

  # POST /saage_forms
  # POST /saage_forms.json
  def create
    @saage_form = SaageForm.new(saage_form_params)
    respond_to do |format|
      if @saage_form.save!      
        format.html { redirect_to my_care_plan_path, notice: 'SAAGE information was successfully created.' }
      else
        render :new
      end
    end
  end

  # PATCH/PUT /saage_forms/1
  # PATCH/PUT /saage_forms/1.json
  def update
  end

  # DELETE /saage_forms/1
  # DELETE /saage_forms/1.json
  def destroy
    @saage_form.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_saage_form
      @saage_form = SaageForm.find(params[:id])
      # fill params
    end

    # Only allow a list of trusted parameters through.
    def saage_form_params
      params.require(:saage_form).permit(
        basic_information_attributes: [
          :id,
          :first_name,
          :last_name,
          :email,
          :bp,
          :height,
          :weight,
          :dob
        ],
        bladder_management_attributes: [
          :response,
          :indwelling_catheter,
          :condom_catheter,
          :suprapubic_catheter,
          :intermittent_catheterization_urethra,
          :intermittent_catheterization_stoma,
          :ileal_conduit,
          :other,
          :other_details
        ]
      )
    end
end
