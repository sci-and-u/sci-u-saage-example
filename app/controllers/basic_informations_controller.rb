class BasicInformationsController < ApplicationController
  before_action :set_basic_information, only: [:show, :destroy]

  # GET /basic_informations
  # GET /basic_informations.json
  def index
    @basic_informations = BasicInformation.all
  end

  # GET /basic_informations/1
  # GET /basic_informations/1.json
  def show
  end

  # POST /basic_informations
  # POST /basic_informations.json
  def create
    @basic_information = BasicInformation.new(basic_information_params)

    respond_to do |format|
      if @basic_information.save
        format.html { redirect_to @basic_information, notice: 'Basic information was successfully created.' }
      else
        format.html { render :index }
      end
    end
  end

  # PATCH/PUT /basic_informations/1
  # PATCH/PUT /basic_informations/1.json
  def update
  end

  # DELETE /basic_informations/1
  # DELETE /basic_informations/1.json
  def destroy
    @basic_information.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: 'Basic information was successfully deleted.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_basic_information
      @basic_information = BasicInformation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def basic_information_params
      params.require(:basic_information).permit(
        :first_name,
        :last_name,
        :email,
        :bp,
        :height,
        :weight,
        :dob
      )
    end
end
