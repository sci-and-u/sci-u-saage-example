class BladderManagementsController < ApplicationController
  before_action :set_bladder_management, only: [:show, :destroy]

  # GET /basic_informations
  # GET /basic_informations.json
  def index
    @bladder_managements = BladderManagement.all
  end


  # GET /basic_informations/1
  # GET /basic_informations/1.json
  def show
  end

  # POST /basic_informations
  # POST /basic_informations.json
  def create
    @bladder_management = BasicInformation.new(bladder_management_params)

    respond_to do |format|
      if @bladder_management.save
        format.html { redirect_to @bladder_management, notice: 'Bladder management information was successfully created.' }
      else
        format.html { render :index }
      end
    end
  end

  # PATCH/PUT /basic_informations/1
  # PATCH/PUT /basic_informations/1.json
  def update
  end

  # DELETE /basic_informations/1
  # DELETE /basic_informations/1.json
  def destroy
    @bladder_management.destroy
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: 'Bladder management information was successfully deleted.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bladder_management
      @bladder_management = BladderManagement.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def bladder_management_params
      params.require(:bladder_management).permit(
        :response,
        :indwelling_catheter,
        :condom_catheter,
        :suprapubic_catheter,
        :intermittent_catheterization_urethra,
        :intermittent_catheterization_stoma,
        :ileal_conduit,
        :other,
        :other_details
      )
    end
end
