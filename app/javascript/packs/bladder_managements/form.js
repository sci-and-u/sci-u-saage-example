function ready() {

  $('input[type=radio][name=bladder_question]').change(function() {
    $('#bladder-routine').hide();
    $('#bladder-details').hide();
    $('#bladder-response').val(this.value);
    if(this.value == 'yes') {
      $('#bladder-routine').show();
    } else if (this.value == 'unsure') {
      $('#bladder-details').show();
    }
  });

  $('#bladder-checkOther').change(function() {
    if(this.checked) {
      $('#bladder-other-details').show();
    } else {
      $('#bladder-other-details').hide();
    }
  });
}
$(document).ready(ready)
$(document).on('turbolinks:load', ready)
