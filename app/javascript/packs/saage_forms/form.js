require("jquery-ui")

function ready() {
  const TOTAL_PAGES = 8;
  $("ul.pagination li a").on('click', function(e){
    if($(this).parent().hasClass("active")) {
      return;
    }
    
    $(".page-item").removeClass("active");
    $(this).parent().addClass("active");

    $(".sections").hide();
    $("#section-" + $(this).parent().attr("id")).show();

  });


  $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    yearRange: '-150:+0'
  });

  $("#section-page-1").show();
}
$(document).ready(ready)

$(document).on('turbolinks:load', ready)
