class BladderManagement < ApplicationRecord
  belongs_to :saage_form
  enum response: %i[yes no unsure]	
end
