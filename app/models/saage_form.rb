class SaageForm < ApplicationRecord
  has_one :basic_information
  has_one :bladder_management
  accepts_nested_attributes_for :basic_information
  accepts_nested_attributes_for :bladder_management  
end
